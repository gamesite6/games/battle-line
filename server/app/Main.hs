{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Control.Monad.Random (evalRand)
import GameState (GameState)
import InitialStateReq (InitialStateReq (..))
import qualified Lib
import Network.Wai (Application)
import Network.Wai.Handler.Warp (run)
import PerformActionReq (PerformActionReq (..))
import PerformActionRes (PerformActionRes (..))
import Servant
import System.Environment (getEnv)
import System.Random (mkStdGen)

main :: IO ()
main = do
  port :: Int <- read <$> getEnv "PORT"
  putStrLn $ "Listening on port " <> show port
  run port app

type GameAPI =
  "initial-state" :> ReqBody '[JSON] InitialStateReq :> Post '[JSON] GameState
    :<|> "perform-action" :> ReqBody '[JSON] PerformActionReq :> Post '[JSON] PerformActionRes

server :: Server GameAPI
server = handleInitialState :<|> handlePerformAction

handleInitialState :: InitialStateReq -> Handler GameState
handleInitialState req@InitialStateReq {players, settings} =
  let seed = InitialStateReq.seed req
      stdGen = mkStdGen seed
   in case evalRand (Lib.initialState settings players) stdGen of
        Just state -> pure state
        Nothing -> throwError $ err400 {errBody = "illegal settings and/or players"}

handlePerformAction :: PerformActionReq -> Handler PerformActionRes
handlePerformAction req@PerformActionReq {performedBy, action, state} =
  let seed = PerformActionReq.seed req
      stdGen = mkStdGen seed
   in case evalRand (Lib.performAction action performedBy state) stdGen of
        Just nextState ->
          pure
            PerformActionRes
              { nextState,
                completed = Lib.isCompleted nextState
              }
        Nothing -> throwError $ err400 {errBody = "illegal game action"}

gameAPI :: Proxy GameAPI
gameAPI = Proxy

app :: Application
app = serve gameAPI server
