{-# LANGUAGE TemplateHaskell #-}

module Action where

import Action.Play
import Card (Card)
import Data.Aeson.TH (Options (fieldLabelModifier), defaultOptions, deriveJSON)

data Action
  = Play Play
  | Draw
  deriving (Eq, Show)

deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''Action
