{-# LANGUAGE TemplateHaskell #-}

module GameState (GameState (..), players, deck, Battle (..)) where

import Card (Card)
import Control.Lens (makeLenses)
import Data.Aeson
  ( Options (fieldLabelModifier),
    defaultOptions,
  )
import Data.Aeson.TH (deriveJSON)
import Phase (Phase (..))
import PlayerId (PlayerId)
import PlayerState (PlayerState)

data Battle = Battle
  { _cards :: ([Card], [Card]),
    _victor :: Maybe PlayerId
  }
  deriving (Eq, Show)

deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''Battle

data GameState = GameState
  { _players :: [PlayerState],
    _deck :: [Card],
    _battles :: [Battle],
    _phase :: Phase
  }
  deriving (Eq, Show)

makeLenses ''GameState
deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''GameState
