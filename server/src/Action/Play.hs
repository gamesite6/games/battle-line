{-# LANGUAGE TemplateHaskell #-}

module Action.Play where

import Card (Card)
import Data.Aeson.TH (defaultOptions, deriveJSON)

data Play = Play
  { _card :: Card,
    _battleIdx :: Int
  }
  deriving (Eq, Show)

deriveJSON defaultOptions ''Play