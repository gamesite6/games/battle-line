{-# LANGUAGE TemplateHaskell #-}

module PerformActionRes where

import Data.Aeson (ToJSON)
import Data.Aeson.TH (defaultOptions, deriveJSON)
import GameState (GameState)

data PerformActionRes = PerformActionRes
  { completed :: Bool,
    nextState :: GameState
  }
  deriving (Eq, Show)

deriveJSON defaultOptions ''PerformActionRes