{-# LANGUAGE TemplateHaskell #-}

module Phase where

import Data.Aeson (defaultOptions)
import Data.Aeson.TH (deriveJSON)
import PlayerId (PlayerId)

data Phase
  = Playing PlayerId
  | Drawing PlayerId
  | GameComplete
  deriving (Eq, Show)

deriveJSON defaultOptions ''Phase
