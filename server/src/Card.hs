{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Card (strength, color, Card, Color, Strength, schottenTottenCards, battleLineCards) where

import Data.Aeson (FromJSON, ToJSON)

newtype Card = Card Int deriving (Eq, Show, Ord, ToJSON, FromJSON)

battleLineCards :: [Card]
battleLineCards = Card <$> [1 .. 60]

schottenTottenCards :: [Card]
schottenTottenCards = filter (\c -> strength c /= Strength 10) battleLineCards

data Color
  = Red
  | Green
  | Blue
  | Yellow
  | Orange
  | Purple
  deriving (Eq, Show, Ord, Enum, Bounded)

colors :: [Color]
colors = [minBound ..]

newtype Strength = Strength Int deriving (Eq, Ord, Show)

strength :: Card -> Strength
strength (Card card) = Strength $ ((card - 1) `mod` 10) + 1

color :: Card -> Color
color (Card card) = case card `mod` 6 of
  0 -> Red
  1 -> Green
  2 -> Blue
  3 -> Yellow
  4 -> Orange
  _ -> Purple
