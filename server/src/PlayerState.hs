{-# LANGUAGE TemplateHaskell #-}

module PlayerState where

import Card (Card)
import Control.Lens (makeLenses)
import Data.Aeson (defaultOptions, fieldLabelModifier)
import Data.Aeson.TH (deriveJSON)
import PlayerId (PlayerId)

data PlayerState = PlayerState
  { __id :: PlayerId,
    _hand :: [Card]
  }
  deriving (Eq, Show)

makeLenses ''PlayerState
deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''PlayerState