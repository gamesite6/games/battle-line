{-# LANGUAGE TemplateHaskell #-}

module InitialStateReq where

import Data.Aeson (defaultOptions)
import Data.Aeson.TH (deriveJSON)
import Data.Set (Set)
import PlayerId (PlayerId)
import Settings (Settings)

data InitialStateReq = InitialStateReq
  { players :: [PlayerId],
    settings :: Settings,
    seed :: Int
  }
  deriving (Eq, Show)

deriveJSON defaultOptions ''InitialStateReq
