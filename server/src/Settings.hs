{-# LANGUAGE TemplateHaskell #-}

module Settings where

import Control.Lens (makeLenses)
import Data.Aeson (defaultOptions, fieldLabelModifier)
import Data.Aeson.TH (deriveJSON)
import Data.Set (Set)
import qualified Data.Set as Set

data Edition = BattleLine | SchottenTotten deriving (Eq, Show)

deriveJSON defaultOptions ''Edition

data Settings = Settings
  { _edition :: Edition,
    _tactics :: Bool,
    _expert :: Bool
  }
  deriving (Eq, Show)

makeLenses ''Settings

deriveJSON defaultOptions {fieldLabelModifier = drop 1} ''Settings

defaultSettings :: Settings
defaultSettings =
  Settings
    { _edition = SchottenTotten,
      _tactics = False,
      _expert = False
    }
