{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module PlayerId where

import Data.Aeson (FromJSON, ToJSON)

newtype PlayerId = PlayerId Int deriving (Eq, Show, ToJSON, FromJSON)
