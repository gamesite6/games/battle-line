module Lib where

import Action (Action)
import Card (battleLineCards, schottenTottenCards)
import Control.Lens
import Control.Monad.Random (MonadRandom)
import Data.Set (Set)
import GameState (Battle (..), GameState (..))
import qualified Phase
import PlayerId (PlayerId)
import PlayerState (PlayerState (..))
import Settings
import System.Random.Shuffle (shuffleM)

initialState :: MonadRandom m => Settings -> [PlayerId] -> m (Maybe GameState)
initialState settings playerIds =
  case playerIds of
    [playerId1, playerId2] -> do
      let (cardsToDraw, deck) =
            case settings ^. edition of
              SchottenTotten -> (6, schottenTottenCards)
              BattleLine -> (7, battleLineCards)

      deck <- shuffleM deck

      let (player1Cards, deck) = splitAt cardsToDraw deck
      let (player2Cards, deck) = splitAt cardsToDraw deck

      let playerStates =
            [ PlayerState {__id = playerId1, _hand = player1Cards},
              PlayerState {__id = playerId2, _hand = player2Cards}
            ]

      startingPlayerId <- head <$> shuffleM playerIds

      pure $
        Just $
          GameState
            { _players = playerStates,
              _deck = deck,
              _battles =
                replicate 9 $
                  Battle
                    { _cards = ([], []),
                      _victor = Nothing
                    },
              _phase = Phase.Playing startingPlayerId
            }
    _ -> pure Nothing

performAction :: MonadRandom m => Action -> PlayerId -> GameState -> m (Maybe GameState)
performAction action activePlayerId gameState = undefined

isCompleted :: GameState -> Bool
isCompleted gameState = undefined
