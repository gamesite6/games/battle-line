{-# LANGUAGE TemplateHaskell #-}

module PerformActionReq where

import Action (Action)
import Data.Aeson (defaultOptions)
import Data.Aeson.TH (deriveJSON)
import GameState
import PlayerId (PlayerId)

data PerformActionReq = PerformActionReq
  { performedBy :: PlayerId,
    action :: Action,
    state :: GameState,
    seed :: Int
  }
  deriving (Eq, Show)

deriveJSON defaultOptions ''PerformActionReq