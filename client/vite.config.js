import path from "path";
import { defineConfig } from "vite";
import svelte from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/battle-line/",
  build: {
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "battle-line",
      name: "Gamesite6_BattleLine",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
