/// <reference types="svelte" />
/// <reference types="vite/client" />

type PlayerId = number;

type CardId = number;

type GameState = any;

type PlayerState = any;

type Phase = any;

type Settings = any;

type Action = any;
